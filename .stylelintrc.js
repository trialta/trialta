module.exports = {
  extends: [
    "@superkoders/stylelint-config",
    "stylelint-prettier/recommended",
    "stylelint-config-prettier",
  ],
};
