import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";

const defaultOptions = {
  watchQuery: {
    fetchPolicy: "no-cache",
    errorPolicy: "ignore",
  },
  query: {
    fetchPolicy: "no-cache",
    errorPolicy: "all",
  },
};

const cache = new InMemoryCache({
  resultCaching: false,
});

const link = createHttpLink({
  //uri: `${process.env.REACT_APP_API_URL}/graphql`,
  uri: 'https://e0s.b58.myftpupload.com/graphql',
});

const client = new ApolloClient({
  link,
  cache,
  defaultOptions,
});

export default client;