import { gql } from "@apollo/client";

export const QUERY_ABOUT = gql`
  query queryAbout {
    latribu(
      first: 50
      where: { dateQuery: {}, orderby: { field: DATE, order: DESC } }
    ) {
      nodes {
        group {
          tvshow
          specialty
          occupation
          name
          employment
          drink
          artist
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    nuestrashistorias(
      first: 50
      where: { dateQuery: {}, orderby: { field: DATE, order: ASC } }
    ) {
      nodes {
        group {
          year
          title
          description
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
  }
`;
