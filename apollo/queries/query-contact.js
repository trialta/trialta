import { gql } from "@apollo/client";

export const QUERY_CONTACT = gql`
  query queryContact {
    contactos {
      nodes {
        group {
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
  }
`;
