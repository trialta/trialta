import { gql } from "@apollo/client";

export const QUERY_COTIZATION = gql`
  query queryCotization {
    cotizaciones {
      nodes {
        group {
          title
          subtitle
          paragraph
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    categories {
      nodes {
        id
        name
        slug
      }
    }
  }
`;
