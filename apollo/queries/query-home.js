import { gql } from "@apollo/client"
import SeoFragment from "./seo/index"

export const QUERY_HOME = gql`
  query queryHome {
    secciones {
      nodes {
        group {
          h1
          button
          subtitle
          title
          url
        }
      }
    }
    homeSliders {
      nodes {
        group {
          title
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
        seo {
          ...SeoFragment
        }
      }
    }
    nuestraCulturas {
      nodes {
        group {
          title
          subtitle
          urlvideo
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
            title
          }
          content
          button
        }
      }
    }
    portafolios {
      nodes {
        id
        group {
          name
          link
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
            title
          }
        }
        categories {
          edges {
            node {
              id
              name
              slug
            }
          }
        }
      }
    }
    trayectorias {
      nodes {
        group {
          achievement
          score
          order
        }
        id
      }
    }
    trayectoriasmain {
      nodes {
        group {
          link
          button
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    tribus(first: 100) {
      nodes {
        group {
          marca
          logo {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
  }
  ${SeoFragment}
`
