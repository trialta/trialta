import { gql } from "@apollo/client";

export const PRODUCT_BY_CATEGORY_SLUG = gql`
  query PRODUCT_BY_CATEGORY_SLUG($slug: String!) {
    proyectos(
      first: 600
      where: {
        dateQuery: {}
        orderby: { field: DATE, order: ASC }
        categoryName: $slug
      }
    ) {
      nodes {
        id
        group {
          title
          subtitle
          url
          image {
            srcSet(size: LARGE)
            sourceUrl(size: LARGE)
          }
        }
        categories {
          edges {
            node {
              name
              slug
              id
            }
          }
        }
      }
    }
    nosotros {
      nodes {
        group {
          title
          subtitle
          paragraph
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    categories {
      nodes {
        id
        name
        slug
      }
    }
    testimoniales {
      nodes {
        group {
          testimonial
          person
          occupation
        }
      }
    }
  }
`;

export const PRODUCT_CATEGORIES_SLUGS = gql`
  query PRODUCT_CATEGORIES_SLUGS {
    proyectos(first: 5000) {
      nodes {
        categories {
          edges {
            node {
              name
              slug
              id
            }
          }
        }
      }
    }
  }
`;
