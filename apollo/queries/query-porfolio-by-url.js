import { gql } from "@apollo/client";

export const PRODUCT_BY_URL = gql`
  query PRODUCT_BY_URL($url: ID!) {
    proyecto(id: $url, idType: URI) {
      id
      group {
        title
        subtitle
        url
        image {
          srcSet(size: LARGE)
        }
        images {
          titlebanner
          subtitlebanner
          parragraphbanner
          imagebanner {
            srcSet(size: LARGE)
          }
          image1 {
            srcSet(size: LARGE)
          }
          image2 {
            srcSet(size: LARGE)
          }
          image3 {
            srcSet(size: LARGE)
          }
          image4 {
            srcSet(size: LARGE)
          }
          image5 {
            srcSet(size: LARGE)
          }
          image6 {
            srcSet(size: LARGE)
          }
          image7 {
            srcSet(size: LARGE)
          }
          image8 {
            srcSet(size: LARGE)
          }
          image9 {
            srcSet(size: LARGE)
          }
          image10 {
            srcSet(size: LARGE)
          }
        }
      }
      categories {
        nodes {
          slug
        }
      }
    }
    nosotros {
      nodes {
        group {
          title
          subtitle
          paragraph
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    categories {
      nodes {
        id
        name
        slug
      }
    }
    testimoniales {
      nodes {
        group {
          testimonial
          person
          occupation
        }
      }
    }
  }
`;

export const PRODUCT_URLS = gql`
  query PRODUCT_URLS {
    proyectos(first: 5000) {
      nodes {
        id
        group {
          title
          url
        }
        categories {
          nodes {
            slug
          }
        }
      }
    }
  }
`;
