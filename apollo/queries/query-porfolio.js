import { gql } from "@apollo/client";

export const QUERY_PORFOLIO = gql`
  query queryPorfolio {
    nosotros {
      nodes {
        group {
          title
          subtitle
          paragraph
          image {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
      }
    }
    categories {
      nodes {
        id
        name
        slug
      }
    }
    proyectos(
      first: 600
      where: { dateQuery: {}, orderby: { field: DATE, order: ASC } }
    ) {
      nodes {
        group {
          title
          subtitle
          image {
            srcSet(size: LARGE)
            sourceUrl(size: LARGE)
          }
          url
        }
        categories {
          edges {
            node {
              name
              slug
              id
            }
          }
        }
      }
    }
    testimoniales {
      nodes {
        group {
          testimonial
          person
          occupation
        }
      }
    }
  }
`;
