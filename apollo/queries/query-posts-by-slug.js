import { gql } from "@apollo/client";
import SeoFragment from "./seo/index";

export const POST_BY_SLUG = gql`
  query Post($slug: ID!) {
    post(id: $slug, idType: SLUG) {
      group {
        excerpt
        category
        featured {
          sourceUrl(size: LARGE)
          srcSet(size: LARGE)
        }
        thumbnail {
          sourceUrl(size: LARGE)
          srcSet(size: LARGE)
        }
      }
      date
      slug
      title(format: RENDERED)
      content(format: RENDERED)
      status
      seo {
        ...SeoFragment
      }
    }
  }
  ${SeoFragment}
`;

export const POST_SLUGS = gql`
  query Posts {
    posts(first: 5000) {
      nodes {
        id
        slug
      }
    }
  }
`;
