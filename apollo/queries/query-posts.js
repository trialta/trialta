import { gql } from "@apollo/client";
import SeoFragment from "./seo/index";

export const QUERY_POSTS = gql`
  query queryPosts {
    posts {
      nodes {
        id
        title(format: RENDERED)
        content(format: RENDERED)
        slug
        date
        status
        group {
          category
          excerpt
          featured {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
          thumbnail {
            sourceUrl(size: LARGE)
            srcSet(size: LARGE)
          }
        }
        seo {
          ...SeoFragment
        }
      }
    }
  }
  ${SeoFragment}
`;
