import Link from "next/link"
import IconArrowRight from "../Icons/IconArrowRight"
import IconPlay from "../Icons/IconPlay"

const renderSwith = (icon) => {
  switch (icon) {
    case "icon-arrow-right":
      return <IconArrowRight color={"#FFED00"} />
    case "icon-play":
      return <IconPlay color={"#FFED00"} />
  }
}

const Button = ({
  name,
  url = "",
  icon = "icon-arrow-right",
  type = "button",
}) => {
  return type === "link" ? (
    <Link href={url}>
      <a className="trl-btn">
        <span>
          {name} {renderSwith(icon)}
        </span>
      </a>
    </Link>
  ) : (
    <button className="trl-btn">
      <span>
        {name} {renderSwith(icon)}
      </span>
    </button>
  )
}

export default Button
