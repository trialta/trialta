import MessengerCustomerChat from "react-messenger-customer-chat"

const Messenger = () => {
  return (
    <MessengerCustomerChat
      pageId="1977327975709702"
      appId="867038447402737"
      language="es_ES"
      themeColor="#2f2f2f"
      minimized={false}
    />
  )
}

export default Messenger
