import React, { useState } from "react"
import Navigation from "../Header/Navigation"
import SocialList from "../Social/SocialList"
import IconPattern from "../../Atoms/Icons/IconPattern"
import IconInfo from "../../Atoms/Icons/IconInfo"
import Button from "../../Atoms/Buttons/Buttons"
import axios from "axios"

const Footer = () => {
  const initialState = {
    input_1: "",
    input_2: "",
    input_6: "",
    input_7: "",
    input_8: "",
  }
  const [contactFields, setContactFields] = useState(initialState)

  const [message, setMessage] = useState("")

  const onChange = (e) => {
    setContactFields({
      ...contactFields,
      [e.target.name]: e.target.value,
    })
  }

  const onSubmit = (e) => {
    e.preventDefault()
    senMessage(
      contactFields.input_1,
      contactFields.input_2,
      contactFields.input_6,
      contactFields.input_7,
      contactFields.input_8
    )
  }

  const clearFields = () => {
    setContactFields({ ...initialState })
  }

  const senMessage = (input_1, input_2, input_6, input_7, input_8) => {
    const config = {
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
    }
    const body = JSON.stringify({
      input_1,
      input_2,
      input_6,
      input_7,
      input_8,
    })

    axios
      .post(
        //`${process.env.NEXT_PUBLIC_GRAVIYFORM_SITE_URL}/1/submissions`,
        'https://e0s.b58.myftpupload.com/wp-json/gf/v2/forms/1/submissions',
        body,
        config
      )
      .then((response) => {
        setMessage(response.data.confirmation_message)
        clearFields()
      })
      .catch((error) => {
        error
      })
  }

  return (
    <footer className="trl-footer">
      <div className="trl-footer__container">
        <div className="pattern">
          <IconPattern color="#2F2F2F" />
        </div>
        <div className="content">
          <h3 className="trl-title">Contacto</h3>
          <div className="trl-form">
            <form onSubmit={onSubmit}>
              <div className="field">
                <label htmlFor="fullname">Nombre completo</label>
                <input
                  id="fullname"
                  type="text"
                  name="input_1"
                  onChange={onChange}
                  value={contactFields.input_1}
                />
              </div>
              <div className="field">
                <label htmlFor="company">Empresa</label>
                <input
                  id="company"
                  type="text"
                  name="input_2"
                  onChange={onChange}
                  value={contactFields.input_2}
                />
              </div>
              <div className="field">
                <label htmlFor="phone">Teléfono</label>
                <input
                  id="phone"
                  type="text"
                  name="input_6"
                  onChange={onChange}
                  value={contactFields.input_6}
                />
              </div>
              <div className="field">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  id="email"
                  name="input_7"
                  onChange={onChange}
                  value={contactFields.input_7}
                />
              </div>
              <div className="field field-message">
                <label htmlFor="message">Mensaje</label>
                <input
                  id="message"
                  type="text"
                  name="input_8"
                  onChange={onChange}
                  value={contactFields.input_8}
                />
              </div>
              <div className="field field-btn">
                <Button
                  name="Consulta"
                  url="/"
                  icon="icon-arrow-right"
                  type="button"
                />
              </div>
            </form>
            <div dangerouslySetInnerHTML={{ __html: message }} />
          </div>
          <nav>
            <Navigation />
          </nav>
        </div>
        <aside>
          <ul className="trl-info">
            <li>
              <IconInfo icon="marker" color={"#2F2F2F"} />
              <div className="trl-address">
                <p>
                  Av. Benavides 2150 <br />
                  Interior 302 <br />
                  Miraflores <br />
                  Lima, Perú
                </p>
              </div>
            </li>
            <li>
              <IconInfo icon="phone" color={"#2F2F2F"} />
              <a className="trl-phone" href="tel:+51 647 5821">
                +51 647 5821
              </a>
            </li>
            <li>
              <IconInfo icon="mail" color={"#2F2F2F"} />
              <div className="trl-email">hola@trialta.pe</div>
            </li>
          </ul>
          <SocialList />
        </aside>
      </div>
      <span>© Trialta 2020. Todos los derechos reservados.</span>
    </footer>
  )
}

export default Footer
