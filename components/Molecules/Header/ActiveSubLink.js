import * as React from "react"
import Link from "next/link"
import { withRouter } from "next/router"
import { Children } from "react"

const ActiveSubLink = withRouter(({ router, children, ...props }) => (
  <Link {...props}>
    {React.cloneElement(Children.only(children), {
      className:
        `${router.asPath.split("/")[2]}` === props.href.split("/")[2]
          ? `active`
          : null,
    })}
  </Link>
))

export default ActiveSubLink
