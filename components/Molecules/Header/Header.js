import React, { createRef } from "react"
import Link from "next/link"
import Logo from "../../Atoms/Icons/Logo"
import SocialList from "../Social/SocialList"
import Menu from "../../Molecules/Header/Menu"

const Header = () => {
  const hamburger = createRef()
  const menu = createRef()

  const openModal = () => {
    menu.current.classList.toggle("open")
    hamburger.current.classList.toggle("active")
  }

  return (
    <header className="trl-header">
      <Link href="/">
        <a className="logo">
          <span>
            <Logo />
          </span>
        </a>
      </Link>
      <button className="hamburger" ref={hamburger} onClick={() => openModal()}>
        <span></span>
      </button>
      <SocialList />
      <Menu refMenu={menu} />
    </header>
  )
}

export default Header
