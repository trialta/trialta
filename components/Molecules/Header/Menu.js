import Navigation from "../Header/Navigation"

const Menu = ({ refMenu }) => {
  return (
    <div className="menu" ref={refMenu}>
      <nav>
        <Navigation />
      </nav>
      <div>
        Contáctanos: <a href="mailto:hola@trialta.pe">hola@trialta.pe</a>
      </div>
    </div>
  )
}

export default Menu
