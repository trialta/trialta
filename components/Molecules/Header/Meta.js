import * as React from "react"

const ViewportMetaLink = () => {
  return (
    <React.Fragment>
      <meta charSet="UTF-8" />
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
        key="viewport-meta"
      />
      <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
      <title>Trialta - Soluciones arquitectónicas</title>
      <meta
        name="description"
        content="Estudio de arquitectura y diseño de interiores. Juntos co-creamos soluciones arquitectónicas"
      />
      <meta
        name="google-site-verification"
        content="TqI9eJP7BDwRGP1Xfm5gZVIDhO9YJpeukWdzBC3igP8"
      />
      <link
        rel="canonical"
        //href={`${process.env.NEXT_PUBLIC_NEXTJS_SITE_URL}`}
        href='http://localhost:3000'
      />
      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content="Trialta - Soluciones Arquitectónicas"
      />
      <meta
        property="og:description"
        content="Estudio de arquitectura y diseño de interiores. Juntos co-creamos soluciones arquitectónicas"
      />
      {/*
        <meta
          property="og:image"
          content={`${process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL}/wp-content/uploads/2020/12/triatal-opengraph.jpg`}
        />
        <meta
          property="og:url"
          content={`${process.env.NEXT_PUBLIC_NEXTJS_SITE_URL}`}
        />
      */}
      <meta property="og:site_name" content="Trialta Group" />
    </React.Fragment>
  )
}

export default ViewportMetaLink
