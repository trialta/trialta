import * as React from "react"
import ActiveLink from "./ActiveLink"

const links = [
  { href: "/", label: "Home" },
  { href: "/nuestra-historia", label: "Nosotros" },
  { href: "/portafolio", label: "Proyectos" },
  { href: "/contacto", label: "Contacto" },
  { href: "/blog", label: "Blog" },
  { href: "/cotizacion", label: "Cotización" },
  { href: "https://docs.google.com/forms/d/18eUzm4O3WxaN_CDTTXks4pD9QpE_iTsYB14ikiAHnaU/edit", label: "Libro de reclamaciones" },
  { href: "https://docs.google.com/forms/d/1nWsm43VYAwPeAddOfvMlxj601DeKHIv3-jpMe0-OAFo/edit", label: "Sugerencias" },
  { href: "/were-hiring", label: "We're Hiring!" },
].map((ActiveLink) => {
  ActiveLink.key = `key-${ActiveLink.href}-${ActiveLink.label}`
  return ActiveLink
})

const Navigation = () => {
  return (
    <ul>
      {links.map(({ key, href, label }) => (
        <li key={key}>
          <ActiveLink href={href}>
            <a>{label}</a>
          </ActiveLink>
        </li>
      ))}
    </ul>
  )
}

export default Navigation
