import Link from "next/link"
import IconSocial from "../../Atoms/Icons/IconSocial"

const SocialList = () => {
  return (
    <ul className="trl-social">
      <li>
        <Link href="https://www.instagram.com/trialta_/">
          <a target="_blank" rel="noopener noreferrer">
            <IconSocial icon="instagram" color={"#2F2F2F"} />
          </a>
        </Link>
      </li>
      <li>
        <Link href="https://www.linkedin.com/company/trialtagroup">
          <a target="_blank" rel="noopener noreferrer">
            <IconSocial icon="linkedin" color={"#2F2F2F"} />
          </a>
        </Link>
      </li>
      <li>
        <Link href="https://www.facebook.com/TrialtaGroup">
          <a target="_blank" rel="noopener noreferrer">
            <IconSocial icon="facebook" color={"#2F2F2F"} />
          </a>
        </Link>
      </li>
      <li>
        <Link href="https://wa.me/51913062377?text=Tengo un proyecto en mente, me pueden ayudar?">
          <a target="_blank" rel="noopener noreferrer">
            <IconSocial icon="whatsapp" color={"#2F2F2F"} />
          </a>
        </Link>
      </li>
    </ul>
  )
}

export default SocialList
