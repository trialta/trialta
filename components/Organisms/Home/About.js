import * as React from "react"
import { useState, createRef, useCallback } from "react"
import IconStep from "../../Atoms/Icons/IconStep"
import IconPlay from "../../Atoms/Icons/IconPlay"
import IconClose from "../../Atoms/Icons/IconClose"
import Embed from "../../Atoms/Youtube/Embed"
import { LazyLoadImage } from "react-lazy-load-image-component"

const About = ({ data }) => {
  const [modal, setModal] = useState({
    showComponent: false,
  })
  const modalAbout = createRef()

  const openModalAbout = useCallback(() => {
    modalAbout.current.classList.add("active")
    setModal({
      showComponent: true,
    })
  })

  const closeModalAbout = useCallback(() => {
    modalAbout.current.classList.remove("active")
  })

  return (
    <React.Fragment>
      {data.nuestraCulturas.nodes.map((data, key) => (
        <section className="trl__sec-aboutus" key={key}>
          <article>
            <div className="trl-bloc">
              <h2 className="trl-title">
                {data.group.title} <strong>{data.group.subtitle}</strong>
              </h2>
              <div
                className="trl-paragraph"
                dangerouslySetInnerHTML={{ __html: data.group.content }}
              ></div>
              <button className="trl-btn" onClick={() => openModalAbout()}>
                <span>
                  {data.group.button} <IconPlay color={"#FFED00"} />
                </span>
              </button>
              <ul className="list-steps">
                <li>
                  <IconStep icon="design" color="#FFED00" />
                  <strong>Diseño</strong>
                </li>
                <li>
                  <IconStep icon="implementation" color="#FFED00" />
                  <strong>Implementación</strong>
                </li>
                <li>
                  <IconStep icon="after-sales" color="#FFED00" />
                  <strong>Postventa</strong>
                </li>
              </ul>
            </div>
            <div className="trl-bloc">
              <figure>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data.group.image.srcSet}
                  alt={data.group.title}
                />
              </figure>
            </div>
          </article>
          <div className="modal" ref={modalAbout}>
            <div className="modal-content">
              {modal.showComponent ? (
                <Embed youtubeId={data.group.urlvideo} />
              ) : null}
              <button className="btn-close" onClick={() => closeModalAbout()}>
                <IconClose color="#FFFFFF" width="12px" />
              </button>
            </div>
          </div>
        </section>
      ))}
    </React.Fragment>
  )
}

export default React.memo(About)
