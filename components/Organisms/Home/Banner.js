import * as React from "react"
import Button from "../../Atoms/Buttons/Buttons"
import Slider from "react-slick"
import ArrowBottom from "../../Atoms/Down/ArrowBottom"
import { LazyLoadImage } from "react-lazy-load-image-component"

const Banner = ({ banner, slider }) => {
  const timing = 7000
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    lazyLoad: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 600,
    autoplaySpeed: timing,
  }

  return (
    <main className="trl__sec-main">
      <article>
        {banner?.nodes.map((data, key) => (
          <React.Fragment key={key}>
            <strong className="trl-title">
              {data?.group?.title}
              <span>{data?.group?.subtitle}</span>
            </strong>
            <h1 className="trl-paragraph">{data?.group?.h1}</h1>
            <Button
              name={data?.group?.button}
              url={data?.group?.url}
              icon="icon-arrow-right"
              type="link"
            />
          </React.Fragment>
        ))}
      </article>
      <div className="sld-banner">
        <Slider {...settings}>
          {slider?.nodes
            .sort((a, b) => (a.order > b.order ? 1 : -1))
            .map((data, key) => (
              <LazyLoadImage
                effect="blur"
                srcSet={data?.group?.image?.srcSet}
                alt={data?.group?.title}
                key={key}
              />
            ))}
        </Slider>
      </div>
      <ArrowBottom color="#ffed00" />
    </main>
  )
}

export default React.memo(Banner)
