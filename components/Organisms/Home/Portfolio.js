import * as React from "react"
import Link from "next/link"
import Button from "../../Atoms/Buttons/Buttons"
import Slider from "react-slick"
import { LazyLoadImage } from "react-lazy-load-image-component"

const Portfolio = ({ projects }) => {
  const timing = 4000
  let categorieUrl = ""
  const portfolio = "portafolio"

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    autoplay: true,
    slidesToShow: 3,
    speed: 600,
    initialSlide: 3,
    autoplaySpeed: timing,
    lazyLoad: true,
  }

  return (
    <section className="trl__sec-portfolio">
      <article>
        <h2 className="trl-title">Portafolio</h2>
        <div className="sld-portfolio">
          <Slider {...settings}>
            {projects?.nodes.map((project) => (
              <div className="item" key={project?.id}>
                <LazyLoadImage
                  effect="blur"
                  srcSet={project?.group?.image?.srcSet}
                  alt={project?.group?.title}
                />
                {project?.categories?.edges.map((categorie) => {
                  categorieUrl = categorie?.node?.slug
                })}
                <Link
                  href={`${portfolio}/${categorieUrl}/${project?.group.link}`}
                >
                  <a>
                    <h3>{project?.group?.name}</h3>
                    <span>
                      {project.categories.edges.map((item) => {
                        return item?.node?.name
                      })}
                      <small>/</small>
                    </span>
                  </a>
                </Link>
              </div>
            ))}
          </Slider>
        </div>
        <div className="trl-btn-group">
          <Button
            name="Explora más proyectos"
            url="/portafolio"
            icon="icon-arrow-right"
            type="link"
          />
        </div>
      </article>
    </section>
  )
}

export default React.memo(Portfolio)
