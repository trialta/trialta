import * as React from "react"
import Button from "../../Atoms/Buttons/Buttons"
import { LazyLoadImage } from "react-lazy-load-image-component"

const Trajectory = ({ trajectories, image }) => {
  return (
    <section className="trl__sec-history">
      <article>
        <ul>
          {trajectories?.nodes.map((data) => (
            <li key={data?.id} className={`item-${data?.group?.order}`}>
              <b>{data?.group?.score}</b>
              <span>{data?.group?.achievement}</span>
            </li>
          ))}
        </ul>

        {image.nodes.map((data, key) => (
          <React.Fragment key={key}>
            <LazyLoadImage
              effect="blur"
              srcSet={data?.group?.image?.srcSet}
              alt={data?.group?.title}
            />
            <div className="trl-btn-group">
              <Button
                name={data?.group?.button}
                url={`/${data?.group?.link}`}
                icon="icon-arrow-right"
                type="link"
              />
            </div>
          </React.Fragment>
        ))}
      </article>
    </section>
  )
}

export default React.memo(Trajectory)
