import * as React from "react"
import Slider from "react-slick"
import { LazyLoadImage } from "react-lazy-load-image-component"

const Tribe = ({ tribes }) => {
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    speed: 4000,
    autoplaySpeed: 10,
    cssEase: "linear",
  }

  return (
    <section className="trl__sec-tribe">
      <article>
        <h2 className="trl-title">
          <span>Tribu</span> <strong>de clientes</strong>
        </h2>
        <Slider {...settings}>
          {tribes?.nodes.map((data, key) => (
            <div className="item" key={key}>
              <LazyLoadImage
                effect="blur"
                src={data?.group?.logo?.sourceUrl}
                alt={data?.group?.brand}
              />
            </div>
          ))}
        </Slider>
      </article>
    </section>
  )
}

export default React.memo(Tribe)
