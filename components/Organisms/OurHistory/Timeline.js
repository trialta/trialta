import * as React from "react"
import { useState, useEffect } from "react"
import Button from "../../Atoms/Buttons/Buttons"
import Slider from "react-slick"
import { LazyLoadImage } from "react-lazy-load-image-component"

const TimeLine = ({ data }) => {
  const [navigation, setNavigation] = useState({
    nav1: null,
    nav2: null,
  })

  let slider1, slider2

  useEffect(() => {
    setNavigation({
      nav1: slider1,
      nav2: slider2,
    })
  }, [])

  return (
    <React.Fragment>
      <section className="trl-our-history">
        <article>
          <h2 className="trl-title">
            {" "}
            Nuestra <strong>Historia</strong>
          </h2>
          <p className="trl-paragraph">
            Contamos con más de 14 años de experiencia en el mercado.
          </p>
        </article>
      </section>
      <section className="trl-timeline">
        <Slider
          asNavFor={navigation.nav1}
          ref={(slider) => (slider2 = slider)}
          slidesToShow={8}
          swipeToSlide={true}
          focusOnSelect={true}
          infinite={false}
        >
          {data.nuestrashistorias.nodes.map((data, key) => (
            <div key={key}>
              <button>
                <span>{data.group.year}</span>
              </button>
            </div>
          ))}
        </Slider>
      </section>
      <section className="trl-timeline-detail">
        <div className="content">
          <Slider
            asNavFor={navigation.nav2}
            ref={(slider) => (slider1 = slider)}
            adaptiveHeight={true}
            fade={true}
            nextArrow={false}
            prevArrow={false}
          >
            {data.nuestrashistorias.nodes.map((data, key) => (
              <div key={key}>
                <article>
                  <div className="content">
                    <h2>{data.group.year}</h2>
                    <h3>{data.group.title}</h3>
                    <p>{data.group.description}</p>
                  </div>
                  {data.group.image.srcSet && data.group.image.srcSet ? (
                    <figure>
                      <LazyLoadImage
                        effect="blur"
                        srcSet={data.group.image.srcSet}
                        alt={data.group.title}
                      />
                    </figure>
                  ) : (
                    ""
                  )}
                </article>
              </div>
            ))}
          </Slider>
        </div>
      </section>
      <marquee behavior="scroll" truespeed="truespeed" scrollamount="10">
        WE ARE HIRING! WE ARE HIRING! WE ARE HIRING! WE ARE HIRING! WE ARE
        HIRING!
      </marquee>
      <section className="trl-our-history trl-werehiring">
        <article>
          <p className="trl-paragraph">
            Recrutamos diseñadores, arquitectos, paisajistas o personas con
            grandes historias.
          </p>

          <div className="trl-btn-group">
            <Button
              name="Postula aquí"
              url="/were-hiring"
              icon="icon-arrow-right"
              type="link"
            />
          </div>
        </article>
      </section>
    </React.Fragment>
  )
}

export default React.memo(TimeLine)
