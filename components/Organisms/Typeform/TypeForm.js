import * as React from "react"
import { useRef, useEffect } from "react"
import * as typeformEmbed from "@typeform/embed"

const TypeForm = () => {
  const typeformRef = useRef(null)

  useEffect(() => {
    typeformEmbed.makeWidget(
      typeformRef.current,
      "https://ale502513.typeform.com/to/kqZiwOMT",
      {
        hideFooter: true,
        hideHeaders: true,
        opacity: 0,
      }
    )
  }, [typeformRef])
  return (
    <div ref={typeformRef} style={{ height: "100vh", width: "100%" }}></div>
  )
}

export default React.memo(TypeForm)
