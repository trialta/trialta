import * as React from "react"
import dynamic from "next/dynamic"
import Header from "../../Molecules/Header/Header"
import Footer from "../../Molecules/Footer/Footer"

const Messenger = dynamic(() => import("../../Atoms/Chat/Messenger"), {
  ssr: false,
})

function getFullHeight() {
  let vh = window.innerHeight * 0.01
  document.documentElement.style.setProperty("--vh", `${vh}px`)
}

if (typeof window !== "undefined") {
  getFullHeight()
}

function Layout({ children }) {
  return (
    <React.Fragment>
      <Header />
      <div className="trl-wrapper">{children}</div>
      <Messenger />
      <Footer />
    </React.Fragment>
  )
}

export default Layout
