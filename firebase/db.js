import firebase from "firebase/app"
import "firebase/firestore"
import "firebase/storage"

const config = {
  apiKey: "AIzaSyCVhkqpLQYRiX1jyXqAdPja554ngeGZtUE",
  authDomain: "trialta-web.firebaseapp.com",
  projectId: "trialta-web",
  storageBucket: "trialta-web.appspot.com",
  messagingSenderId: "479032872015",
  appId: "1:479032872015:web:ae093f970b528c35d4ae3a",
  measurementId: "G-B9CCTHDNRC",
}
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const storage = firebase.storage()

export { storage, firebase as default }
