require("dotenv").config();
const path = require("path");
const Dotenv = require("dotenv-webpack");
const withOffline = require("next-offline");

module.exports = withOffline({
  workboxOpts: {
    runtimeCaching: [
      {
        urlPattern: /.jpg$/,
        handler: "CacheFirst",
      },
      {
        urlPattern: /api/,
        handler: "NetworkFirst",
        options: {
          cacheableResponse: {
            statuses: [0, 200],
            headers: {
              "x-test": "true",
            },
          },
        },
      },
    ],
  },

  webpack(config) {
    const conf = config;
    conf.plugins = conf.plugins || [];
    conf.plugins = [
      ...conf.plugins,
      new Dotenv({
        path: path.join(__dirname, ".env"),
        systemvars: true,
      }),
    ];
    return conf;
  },
});

const withSass = require("@zeit/next-sass");
module.exports = withSass();
const withImages = require("next-images");
module.exports = withImages();

module.exports = {
  trailingSlash: true,
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };

    return config;
  },
};
