import Layout from "components/Templates/Layout/Layout"
import * as React from "react"
import Button from "components/Atoms/Buttons/Buttons"

const Custom404 = () => {
  return (
    <div className="page-error">
      <Layout>
        <div className="page-error__container">
          <strong>404</strong>
          <p>Página no encontrada</p>
          <Button
            name="Ir a inicio"
            url="/"
            icon="icon-arrow-right"
            type="link"
          />
        </div>
      </Layout>
    </div>
  )
}

export default Custom404
