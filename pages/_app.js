import { ApolloProvider } from "@apollo/client"
import client from "apollo/client"
import dynamic from "next/dynamic"
import "scss/app.scss"
const TopProgressBar = dynamic(
  () => {
    return import("components/Molecules/Progress/TopProgressBar")
  },
  { ssr: false }
)

function App({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <TopProgressBar />
      <Component {...pageProps} />;
    </ApolloProvider>
  )
}

export default App
