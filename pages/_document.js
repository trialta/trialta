import Document, { Html, Head, Main, NextScript } from "next/document"
import ViewportMetaLink from "components/Molecules/Header/Meta"
class TrlDocument extends Document {
  render() {
    return (
      <Html lang="es">
        <Head>
          <ViewportMetaLink />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default TrlDocument
