import Head from "next/head"
import Layout from "../../components/Templates/Layout/Layout"
import IconArrowRight from "../../components/Atoms/Icons/IconArrowRight"
import client from "../../apollo/client"
import {
  POST_BY_SLUG,
  POST_SLUGS,
} from "../../apollo/queries/query-posts-by-slug"
import { isEmpty } from "lodash"
import { useRouter } from "next/router"
import Link from "next/link"
import { LazyLoadImage } from "react-lazy-load-image-component"
import Seo from "../../components/Templates/Seo/index"
import { sanitize } from "../../utils/functions"

const Detail = ({ data }) => {
  const router = useRouter()
  const detail = data?.post?.post

  if (router.isFallback) {
    return <div>Loading...</div>
  }

  return (
    <Layout>
      <Seo seo={detail?.seo} uri={detail?.uri} />
      <Head>
        {detail?.seo?.schemaDetails && (
          <script
            type="application/ld+json"
            className="yoast-schema-graph"
            key="yoastSchema"
            dangerouslySetInnerHTML={{
              __html: sanitize(detail.seo.schemaDetails),
            }}
          />
        )}
      </Head>
      {detail.status === "publish" ? (
        <div className="trl-blog-detail">
          <figure className="trl-image-blog">
            <LazyLoadImage
              effect="blur"
              srcSet={detail?.group?.featured?.srcSet}
              alt={detail?.title}
            />
          </figure>
          <main className="trl-detail">
            <div className="info">
              <nav className="breadcrumb">
                <ul>
                  <li>
                    <Link href="/blog">
                      <a>Blog</a>
                    </Link>
                  </li>
                  /
                  <li>
                    <span>{detail.title}</span>
                  </li>
                </ul>
              </nav>
              <div className="top">
                <h1 className="title">{detail?.title}</h1>
                <div className="date">
                  {new Intl.DateTimeFormat("es-ES", {
                    year: "numeric",
                    month: "long",
                    day: "2-digit",
                  }).format(Date.parse(detail?.date))}
                </div>
              </div>
              <div className="content">
                <div
                  className="inner-content"
                  dangerouslySetInnerHTML={{ __html: detail.content }}
                ></div>
              </div>
              <div className="bottom">
                <Link href="/blog">
                  <a className="trl-btn btn-back">
                    <span>
                      <IconArrowRight color={"#FFED00"} />
                      Volver
                    </span>
                  </a>
                </Link>
              </div>
            </div>
          </main>
        </div>
      ) : (
        ""
      )}
    </Layout>
  )
}

export default Detail

export async function getStaticProps(context) {
  const {
    params: { slug },
  } = context

  const { data } = await client.query({
    query: POST_BY_SLUG,
    variables: { slug },
  })

  return {
    props: {
      data: {
        post: data,
      },
    },
    revalidate: 1,
  }
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: POST_SLUGS,
  })

  const pathsData = []

  data?.posts?.nodes &&
    data?.posts?.nodes.map((post) => {
      if (!isEmpty(post?.slug)) {
        pathsData.push({ params: { slug: post?.slug } })
      }
    })

  return {
    paths: pathsData,
    fallback: true,
  }
}
