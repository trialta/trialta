import Head from "next/head"
import Link from "next/link"
import Layout from "../../components/Templates/Layout/Layout"
import client from "../../apollo/client"
import { QUERY_POSTS } from "../../apollo/queries/query-posts"
import { LazyLoadImage } from "react-lazy-load-image-component"
import ArrowBottom from "../../components/Atoms/Down/ArrowBottom"
import IconArrowRight from "../../components/Atoms/Icons/IconArrowRight"
import Seo from "../../components/Templates/Seo/index"
import { sanitize } from "../../utils/functions"

const Blog = ({ data }) => {
  const blog = data?.posts?.posts?.nodes
  let getParamSeo = ""

  blog.map((data) => {
    if (data?.status === "publish" && data?.group?.category === "principal") {
      getParamSeo = data
    }
  })

  return (
    <div className="page-blog">
      <Layout>
        <Seo seo={getParamSeo?.seo} uri={getParamSeo?.uri} />
        <Head>
          {getParamSeo?.seo?.schemaDetails && (
            <script
              type="application/ld+json"
              className="yoast-schema-graph"
              key="yoastSchema"
              dangerouslySetInnerHTML={{
                __html: sanitize(getParamSeo?.seo?.schemaDetails),
              }}
            />
          )}
        </Head>
        <main className="trl-banner">
          {blog.map((data) =>
            data.status === "publish" && data.group.category === "principal" ? (
              <article key={data.id}>
                <h2 className="trl-title">
                  <div dangerouslySetInnerHTML={{ __html: data?.title }}></div>
                </h2>
                <div
                  dangerouslySetInnerHTML={{ __html: data.group?.excerpt }}
                  className="trl-paragraph"
                ></div>
                <div className="trl-group-btn">
                  <Link href={`/blog/${data?.slug}`}>
                    <a className="trl-btn">
                      <span>
                        Ver más <IconArrowRight color={"#FFED00"} />
                      </span>
                    </a>
                  </Link>
                </div>
                <figure>
                  <LazyLoadImage
                    effect="blur"
                    srcSet={data?.group?.featured?.srcSet}
                    alt={data?.title}
                    key={data.id}
                  />
                </figure>
              </article>
            ) : null
          )}
          <ArrowBottom color="#ffed00" />
        </main>
        <div className="trl-blog">
          <div className="trl-blog-content">
            <h2 className="trl-title">BLOG</h2>
            <div className="posts-list">
              {blog?.map((data) =>
                data.status === "publish" ? (
                  <article key={data.id}>
                    <figure>
                      <LazyLoadImage
                        effect="blur"
                        srcSet={data?.group?.thumbnail?.srcSet}
                        alt={data?.title}
                        key={data.id}
                      />
                    </figure>
                    <h2>
                      <div
                        dangerouslySetInnerHTML={{ __html: data?.title }}
                      ></div>
                    </h2>
                    <div className="date">
                      {new Intl.DateTimeFormat("es-ES", {
                        year: "numeric",
                        month: "numeric",
                        day: "2-digit",
                      }).format(Date.parse(data?.date))}
                    </div>
                    <div
                      dangerouslySetInnerHTML={{ __html: data.group?.excerpt }}
                      className="trl-paragraph"
                    ></div>
                    <Link href={`/blog/${data?.slug}`}>
                      <a className="trl-btn">
                        <span>
                          Ver más <IconArrowRight color={"#FFED00"} />
                        </span>
                      </a>
                    </Link>
                  </article>
                ) : null
              )}
            </div>
          </div>
        </div>
      </Layout>
    </div>
  )
}

export default Blog

export async function getStaticProps() {
  const { data } = await client.query({
    query: QUERY_POSTS,
  })

  return {
    props: {
      data: {
        posts: data,
      },
    },
    revalidate: 1,
  }
}
