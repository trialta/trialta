import * as React from "react"
import Layout from "../../components/Templates/Layout/Layout"
import client from "../../apollo/client"
import { QUERY_CONTACT } from "../../apollo/queries/query-contact"

const Contacto = ({ data }) => {
  return (
    <div className="trl-page-contact">
      <Layout>
        {data?.contacts?.contactos?.nodes.map((data, key) => (
          <React.Fragment key={key}>
            <img srcSet={data?.group?.image?.srcSet} alt={data?.group?.title} />
          </React.Fragment>
        ))}
      </Layout>
    </div>
  )
}

export default Contacto

export async function getStaticProps() {
  const { data } = await client.query({
    query: QUERY_CONTACT,
  })

  return {
    props: {
      data: {
        contacts: data,
      },
    },
    revalidate: 1,
  }
}
