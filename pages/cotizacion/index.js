import * as React from "react"
import { useState } from "react"
import client from "../../apollo/client"
import { QUERY_COTIZATION } from "../../apollo/queries/query-cotization"
import Layout from "../../components/Templates/Layout/Layout"
import { storage } from "../../firebase/db"
import { LazyLoadImage } from "react-lazy-load-image-component"
import IconArrowRight from "../../components/Atoms/Icons/IconArrowRight"
import ArrowBottom from "../../components/Atoms/Down/ArrowBottom"
import axios from "axios"

const Cotizacion = ({ data }) => {
  const initialState = {
    input_1: "",
    input_2: "",
    input_3: "",
    input_4: "",
    input_5: "",
    input_6: "",
    input_7: "",
    input_8: "",
  }

  const [urlImage, setUrlImage] = useState("")
  const [percentage, setPercentage] = useState(0)
  const [progress, setProgress] = useState(0)
  const [cotizationFields, setCotizationFields] = useState(initialState)
  const [message, setMessage] = useState("")

  const onChange = (e) => {
    if (e.target.type !== "file") {
      setCotizationFields({
        ...cotizationFields,
        [e.target.name]: e.target.value,
      })
    } else {
      const [input_7] = [e.target.name]
      const image = e.target.files[0]
      const upload = storage.ref(`/images/${image.name}`).put(image)

      upload.on(
        "state_changed",
        (snapshot) => {
          setPercentage(snapshot.bytesTransferred / snapshot.totalBytes) * 100
          setProgress(percentage)
        },
        (error) => {
          error.message
        },
        () => {
          storage
            .ref("images")
            .child(image.name)
            .getDownloadURL()
            .then((urlImage) => {
              setProgress(100)
              setUrlImage(urlImage)
              setCotizationFields({
                ...cotizationFields,
                [input_7]: urlImage,
              })
            })
        }
      )
    }
  }

  function onSubmit(e) {
    e.preventDefault()

    sendMessage(
      cotizationFields.input_1,
      cotizationFields.input_2,
      cotizationFields.input_3,
      cotizationFields.input_4,
      cotizationFields.input_5,
      cotizationFields.input_6,
      cotizationFields.input_7,
      cotizationFields.input_8
    )
  }

  const clearFields = () => {
    setCotizationFields({ ...initialState })
  }

  const sendMessage = (
    input_1,
    input_2,
    input_3,
    input_4,
    input_5,
    input_6,
    input_7,
    input_8
  ) => {
    const config = {
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
    }

    const body = JSON.stringify({
      input_1,
      input_2,
      input_3,
      input_4,
      input_5,
      input_6,
      input_7,
      input_8,
    })

    axios
      .post(
        //`${process.env.NEXT_PUBLIC_GRAVIYFORM_SITE_URL}/1/submissions`,
        'https://e0s.b58.myftpupload.com/wp-json/gf/v2/forms/1/submissions',
        body,
        config
      )
      .then((response) => {
        setMessage(response.data.confirmation_message)
        clearFields()
      })
      .catch((error) => {
        error
      })
  }

  return (
    <Layout>
      <React.Fragment>
        {data?.cotization?.cotizaciones?.nodes.map((data, key) => (
          <main className="trl-banner" key={key}>
            <article>
              <h2 className="trl-title">
                {data.group.title}
                <strong>{data.group.subtitle}</strong>
              </h2>
              <p className="trl-paragraph">{data.group.paragraph}</p>
              <figure>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data.group.image.srcSet}
                  alt={data.group.title}
                />
              </figure>
            </article>
            <ArrowBottom color="#ffed00" />
          </main>
        ))}
      </React.Fragment>
      <section className="frm-cotizacion">
        <form onSubmit={onSubmit}>
          <div className="bloc">
            <h3>Información personal</h3>
            <div className="field">
              <label htmlFor="c-fullname">Nombre completo</label>
              <input
                id="c-fullname"
                type="text"
                name="input_1"
                onChange={(e) => onChange(e)}
                value={setCotizationFields.input_1}
              />
            </div>
            <div className="field">
              <label htmlFor="c-phone">Teléfono</label>
              <input
                id="c-phone"
                type="text"
                name="input_2"
                onChange={(e) => onChange(e)}
                value={setCotizationFields.input_2}
              />
            </div>
            <div className="field">
              <label htmlFor="c-email">Email</label>
              <input
                id="c-email"
                type="email"
                name="input_3"
                onChange={(e) => onChange(e)}
                value={setCotizationFields.input_3}
              />
            </div>
          </div>
          <div className="bloc">
            <h3>Sobre tu proyecto</h3>
            <div className="field">
              <label htmlFor="c-typedesign">Tipo de diseño</label>
              <select
                name="tipo"
                id="c-type"
                name="input_4"
                onChange={(e) => onChange(e)}
                defaultValue={setCotizationFields.input_4}
              >
                <option value="0">Selecciona un tipo</option>
                {data?.cotization?.categories?.nodes.map((category) => (
                  <option key={category.id} value={category.name}>
                    {category.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="field">
              <label htmlFor="c-m2">M²</label>
              <input
                id="c-m2"
                type="text"
                name="input_5"
                onChange={(e) => onChange(e, e.target.value)}
                value={setCotizationFields.input_5}
              />
            </div>
            <div className="field">
              <label htmlFor="c-comment">Comentarios</label>
              <input
                type="text"
                id="c-comment"
                name="input_6"
                onChange={(e) => onChange(e)}
                value={setCotizationFields.input_6}
              />
            </div>
          </div>
          <div className="bloc btn-group">
            <div className="w-btn">
              <label>
                <div className="trl-btn">
                  <span>
                    Subir planos <IconArrowRight color={"#FFED00"} />
                  </span>
                </div>
                <input
                  type="file"
                  name="input_7"
                  onChange={(e) => onChange(e)}
                  value={setCotizationFields.input_7}
                />
              </label>
              <small>
                Puedes subir 1 archivo .PDF o un .zip con varios documentos
              </small>
              <progress value={progress} max="100"></progress>
            </div>
            <div className="w-btn">
              <label>
                <div className="trl-btn">
                  <span>
                    Subir fotografías <IconArrowRight color={"#FFED00"} />
                  </span>
                </div>
                <input
                  type="file"
                  name="input_8"
                  onChange={(e) => onChange(e)}
                  value={setCotizationFields.input_8}
                />
              </label>
              <small>
                Puedes subir una imagen o un .zip con varios imágenes
              </small>
              <progress value={progress} max="100"></progress>
            </div>
            <button className="w-btn trl-btn trl-btn-secondary">
              <span>
                ¡Cotiza ya! <IconArrowRight color="#FFED00" />
              </span>
            </button>
          </div>
          <br />
          <br />
          <div dangerouslySetInnerHTML={{ __html: message }} />
        </form>
      </section>
    </Layout>
  )
}

export default Cotizacion

export async function getStaticProps() {
  const { data } = await client.query({
    query: QUERY_COTIZATION,
  })

  return {
    props: {
      data: {
        cotization: data,
      },
    },
    revalidate: 1,
  }
}
