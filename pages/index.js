import * as React from "react"
import Head from "next/head"
import { isEmpty } from "lodash"
import client from "apollo/client"
import { QUERY_HOME } from "apollo/queries/query-home"
import Layout from "components/Templates/Layout/Layout"
import Banner from "components/Organisms/Home/Banner"
import About from "components/Organisms/Home/About"
import Portfolio from "components/Organisms/Home/Portfolio"
import Trajectory from "components/Organisms/Home/Trajectory"
import Tribe from "components/Organisms/Home/Tribe"

const Home = ({ data }) => {
  const dataHome = data?.home

  if (isEmpty(data)) {
    return null
  }

  return (
    <Layout>
      <Head>
        <script type="application/ld+json">{`
          {
            "@context":"https://schema.org/",
            "@type":"WebSite",
            "name":"Trialta.pe",
            "url":"Trialta",
            "description":"Soluciones arquitectónicas.",
            "email":"hola@trialta.pe",
            "telephone":"+51 547 5821",
            "sameAs":[
              "https://www.facebook.com/TrialtaGroup",
              "https://www.instagram.com/trialta_/",
              "https://www.linkedin.com/company/trialtagroup/"
            ],
            "aggregateRating":{"@type":"AggregateRating",
            "address":{
              "@type":"PostalAddress",
              "addressLocality":"Lima",
              "postalCode":"Miraflores 15048",
              "streetAddress":"Av. Alfredo Benavides 2150, Miraflores 15048, Perú"
            }
          }
        `}</script>
      </Head>
      <Banner banner={dataHome?.secciones} slider={dataHome?.homeSliders} />
      <About data={dataHome} />
      <Portfolio projects={dataHome?.portafolios} />
      <Trajectory
        trajectories={dataHome?.trayectorias}
        image={dataHome?.trayectoriasmain}
      />
      <Tribe tribes={dataHome?.tribus} />
    </Layout>
  )
}

export default Home

export async function getStaticProps() {
  const { data } = await client.query({
    query: QUERY_HOME,
  })

  return {
    props: {
      data: {
        home: data,
      },
    },
    revalidate: 1,
  }
}
