import * as React from "react"
import client from "../../apollo/client"
import { QUERY_ABOUT } from "../../apollo/queries/query-about"
import { LazyLoadImage } from "react-lazy-load-image-component"
import Layout from "../../components/Templates/Layout/Layout"
import TimeLine from "../../components/Organisms/OurHistory/Timeline"

const OurHistory = ({ data }) => {
  return (
    <Layout>
      <div className="trl-banner trl-tribu">
        <article>
          <h1 className="trl-title">
            La <strong>Tribu</strong>
          </h1>
          <p className="trl-paragraph">
            Hay muchos significados de Tribu. Para nosotros es estar
            sincronizados y trabajar en equipo.
          </p>
        </article>
      </div>
      <section className="trl-team">
        <div className="trl-team-list">
          {data?.about?.latribu?.nodes.map((data, key) => (
            <article key={key}>
              <div className="content">
                <h2>{data?.group?.name}</h2>
                <span>{data?.group?.employment}</span>
              </div>
              <figure>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data?.group?.image?.srcSet}
                  alt={data?.group?.title}
                />
              </figure>
              <ul>
                <li>
                  <strong>Profesión</strong>
                  <span>{data?.group?.occupation}</span>
                </li>
                <li>
                  <strong>Especialidad</strong>
                  <span>{data?.group?.specialty}</span>
                </li>
                <li>
                  <strong>Serie de TV</strong>
                  <span>{data?.group?.tvshow}</span>
                </li>
                <li>
                  <strong>Artista</strong>
                  <span>{data?.group?.artist}</span>
                </li>
                <li>
                  <strong>Drink</strong>
                  <span>{data?.group?.drink}</span>
                </li>
              </ul>
            </article>
          ))}
        </div>
      </section>
      <TimeLine data={data?.about} />
    </Layout>
  )
}

export default OurHistory

export async function getStaticProps() {
  //export async function getServerSideProps() {
  const { data, loading, networkStatus } = await client.query({
    query: QUERY_ABOUT,
  })

  return {
    props: {
      data: {
        about: data,
      },
    },
    revalidate: 1,
  }
}
