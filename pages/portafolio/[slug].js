import * as React from "react"
import { useState, useEffect, createRef, useCallback } from "react"
import client from "../../apollo/client"
import Layout from "../../components/Templates/Layout/Layout"
import IconClose from "../../components/Atoms/Icons/IconClose"
import Slider from "react-slick"
import { LazyLoadImage } from "react-lazy-load-image-component"
import ActiveSubLink from "../../components/Molecules/Header/ActiveSubLink"
import Link from "next/link"
import { isEmpty } from "lodash"
import { useRouter } from "next/router"
import {
  PRODUCT_BY_CATEGORY_SLUG,
  PRODUCT_CATEGORIES_SLUGS,
} from "../../apollo/queries/query-porfolio-by-slug"

const CategorySingle = ({ data }) => {
  const router = useRouter()
  const timing = 10000
  const settings = {
    dots: true,
    className: "center",
    centerMode: true,
    centerPadding: "0px",
    infinite: true,
    autoplay: true,
    slidesToShow: 1,
    speed: 600,
    autoplaySpeed: timing,
  }

  if (router.isFallback) {
    return (
      <Layout>
        <div>Loading...</div>
      </Layout>
    )
  }

  const [navigation, setNavigation] = useState({
    nav1: null,
    nav2: null,
  })
  const [modal, setModal] = useState({
    showComponent: false,
  })
  const modalProject = createRef()

  let slider1, slider2

  const openModalProject = useCallback(() => {
    modalProject.current.classList.add("active")
    setModal({
      showComponent: true,
    })
  })

  const closeModalProject = useCallback(() => {
    modalProject.current.classList.remove("active")
  })

  let currentCategoryLink = ""

  useEffect(() => {
    setNavigation({
      nav1: slider1,
      nav2: slider2,
    })
  }, [])

  return (
    <Layout>
      <React.Fragment>
        {data?.porfolio?.nosotros?.nodes.map((data, key) => (
          <main className="trl-banner" key={key}>
            <article>
              <h2 className="trl-title">
                {data?.group?.title}
                <strong>{data?.group?.subtitle}</strong>
              </h2>
              <p className="trl-paragraph">{data?.group?.paragraph}</p>
              <figure>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data?.group?.image?.srcSet}
                  alt={data?.group?.title}
                  key={key}
                />
              </figure>
            </article>
          </main>
        ))}
        <nav className="trl-filter">
          <ul>
            <li>
              <Link href="/portafolio">
                <a>Todos</a>
              </Link>
            </li>
            {data?.porfolio?.categories?.nodes.map((category) => (
              <li key={category.id}>
                <ActiveSubLink href={`/portafolio/${category.slug}`}>
                  <a>{category.name}</a>
                </ActiveSubLink>
              </li>
            ))}
          </ul>
        </nav>
        <div className="trl-grid">
          <div className="trl-projects">
            {data?.porfolio?.proyectos?.nodes?.map((data, key) => (
              <article key={key}>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data?.group?.image?.srcSet}
                  alt={data?.group?.title}
                  key={key}
                />
                <div className="trl-content">
                  <div>
                    <h3>
                      {data?.categories?.edges.map((propiedad) => {
                        currentCategoryLink = propiedad?.node?.slug
                      })}
                      <Link
                        href={`/portafolio/${currentCategoryLink}/${data?.group?.url}`}
                      >
                        <a>{data?.group?.title}</a>
                      </Link>
                    </h3>
                    <p>{data?.group?.subtitle}</p>
                  </div>
                </div>
              </article>
            ))}
          </div>

          <div className="modal" ref={modalProject}>
            <div className="modal-content">
              {modal.showComponent ? (
                <Slider
                  asNavFor={navigation.nav1}
                  ref={(slider) => (slider2 = slider)}
                  slidesToShow={1}
                  swipeToSlide={true}
                  focusOnSelect={true}
                  infinite={true}
                >
                  {data?.porfolio?.proyectos?.nodes?.map((data, key) => (
                    <div key={key}>
                      <div className="c-slider">
                        <LazyLoadImage
                          effect="blur"
                          srcSet={data?.group?.image?.srcSet}
                          alt={data?.group?.title}
                          key={key}
                        />
                      </div>
                      <div className="modal-title">
                        <h3>{data.group.title}</h3>
                      </div>
                    </div>
                  ))}
                </Slider>
              ) : null}
              <button className="btn-close" onClick={() => closeModalProject()}>
                <IconClose color="#FFFFFF" width="12px" />
              </button>
            </div>
          </div>

          <div className="trl-lines">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
        <div className="trl-testimonials">
          <h3 className="trl-title">Testimoniales</h3>
          <div className="sld-testimonials">
            <Slider {...settings}>
              {data?.porfolio?.testimoniales?.nodes.map((data, key) => (
                <article className="item" key={key}>
                  <p>{data?.group?.testimonial}</p>
                  <strong>{data?.group?.person}</strong>
                  <span>{data?.group?.occupation}</span>
                </article>
              ))}
            </Slider>
          </div>
        </div>
      </React.Fragment>
    </Layout>
  )
}
export default CategorySingle

export async function getStaticProps(context) {
  const {
    params: { slug },
  } = context

  const { data } = await client.query({
    query: PRODUCT_BY_CATEGORY_SLUG,
    variables: { slug },
  })

  return {
    props: {
      data: {
        porfolio: data,
      },
    },
    revalidate: 1,
  }
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: PRODUCT_CATEGORIES_SLUGS,
  })

  const pathsData = []

  data?.proyectos?.nodes.map((proyecto) =>
    proyecto?.categories?.edges.map((propiedad) => {
      if (!isEmpty(propiedad?.node?.slug)) {
        return pathsData.push({ params: { slug: propiedad?.node?.slug } })
      }
    })
  )

  return {
    paths: pathsData,
    fallback: true,
  }
}
