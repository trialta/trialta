import * as React from "react"
import { useState, useEffect, createRef, useCallback } from "react"
import client from "../../../apollo/client"
import Layout from "../../../components/Templates/Layout/Layout"
import IconClose from "../../../components/Atoms/Icons/IconClose"
import Slider from "react-slick"
import { LazyLoadImage } from "react-lazy-load-image-component"
import ActiveSubSubLink from "../../../components/Molecules/Header/ActiveSubSubLink"
import Link from "next/link"
import { isEmpty } from "lodash"
import { useRouter } from "next/router"
import {
  PRODUCT_BY_URL,
  PRODUCT_URLS,
} from "../../../apollo/queries/query-porfolio-by-url"

const ProjectSingle = ({ data }) => {
  const collection = data?.proyecto?.proyecto?.group?.images;
  const arrImages = new Array(
    collection?.image1,
    collection?.image2,
    collection?.image3,
    collection?.image4,
    collection?.image5,
    collection?.image6,
    collection?.image7,
    collection?.image8,
    collection?.image9,
    collection?.image10
  )

  const router = useRouter();

  const timing = 10000
  const settings = {
    dots: true,
    className: "center",
    centerMode: true,
    centerPadding: "0px",
    infinite: true,
    autoplay: true,
    slidesToShow: 1,
    speed: 600,
    autoplaySpeed: timing,
  }

  if (router.isFallback) {
    return (
      <Layout>
        <div>Loading...</div>
      </Layout>
    )
  }

  const [navigation, setNavigation] = useState({
    nav1: null,
    nav2: null,
  })
  const [modal, setModal] = useState({
    showComponent: false,
  })
  const modalProject = createRef()

  let slider1, slider2

  const openModalProject = useCallback(() => {
    modalProject.current.classList.add("active")
    setModal({
      showComponent: true,
    })
  })

  const closeModalProject = useCallback(() => {
    modalProject.current.classList.remove("active")
  })

  useEffect(() => {
    setNavigation({
      nav1: slider1,
      nav2: slider2,
    })
  }, [])

  return (
    <Layout>
      <React.Fragment>
        <main className="trl-banner">
          <article>
            <h2 className="trl-title">
              {collection?.titlebanner}
              <strong>{collection?.subtitlebanner}</strong>
            </h2>
            <p className="trl-paragraph">{collection?.parragraphbanner}</p>
            <figure>
              <LazyLoadImage
                effect="blur"
                srcSet={collection?.imagebanner?.srcSet}
                alt={collection?.titlebanner}
                key={1}
              />
            </figure>
          </article>
        </main>
        <nav className="trl-filter">
          <ul>
            <li>
              <Link href="/portafolio">
                <a>Todos</a>
              </Link>
            </li>
            {data?.proyecto?.categories?.nodes.map((category) => (
              <li key={category.id}>
                <ActiveSubSubLink href={`/portafolio/${category.slug}`}>
                  <a>{category.name}</a>
                </ActiveSubSubLink>
              </li>
            ))}
          </ul>
        </nav>
        <div className="trl-grid">
          <div className="trl-projects">
            {arrImages.map(
              (image, key) =>
                image?.srcSet && (
                  <article onClick={() => openModalProject()} key={key}>
                    <LazyLoadImage
                      effect="blur"
                      srcSet={image?.srcSet}
                      key={key}
                    />
                  </article>
                )
            )}
          </div>
          <div className="modal" ref={modalProject}>
            <div className="modal-content">
              {modal.showComponent ? (
                <Slider
                  asNavFor={navigation.nav1}
                  ref={(slider) => (slider2 = slider)}
                  slidesToShow={1}
                  swipeToSlide={true}
                  focusOnSelect={true}
                  infinite={true}
                >
                  {arrImages.map(
                    (image, key) =>
                      image?.srcSet && (
                        <div key={key}>
                          <div className="c-slider">
                            <LazyLoadImage
                              effect="blur"
                              srcSet={image?.srcSet}
                              key={key}
                            />
                          </div>
                        </div>
                      )
                  )}
                </Slider>
              ) : null}
              <button className="btn-close" onClick={() => closeModalProject()}>
                <IconClose color="#FFFFFF" width="12px" />
              </button>
            </div>
          </div>
          <div className="trl-lines">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
        <div className="trl-testimonials">
          <h3 className="trl-title">Testimoniales</h3>
          <div className="sld-testimonials">
            <Slider {...settings}>
              {data?.proyecto?.testimoniales?.nodes.map((data, key) => (
                <article className="item" key={key}>
                  <p>{data?.group?.testimonial}</p>
                  <strong>{data?.group?.person}</strong>
                  <span>{data?.group?.occupation}</span>
                </article>
              ))}
            </Slider>
          </div>
        </div>
      </React.Fragment>
    </Layout>
  )
}

export default ProjectSingle

export async function getStaticProps(context) {
  const {
    params: { slug, url },
  } = context

  try {
    const { data } = await client.query({
      query: PRODUCT_BY_URL,
      variables: { slug, url },
    })

    return {
      props: {
        data: {
          proyecto: data || null,
        },
      },
      revalidate: 1,
    }
  } catch (err) {
    return { props: { errors: err.message } }
  }
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: PRODUCT_URLS,
  })

  const pathsData = []

  let pathSlug = "";
  data?.proyectos?.nodes &&
    data?.proyectos?.nodes?.map((proyecto) => {
      proyecto &&
        proyecto?.categories?.nodes.map((ele) => {
          pathSlug = ele?.slug
        })
      if (!isEmpty(proyecto?.group?.url)) {
        pathsData.push({
          params: { slug: pathSlug, url: proyecto?.group?.url.toString() },
        })
      }
    })

  return {
    paths: pathsData,
    fallback: true,
  }
}
