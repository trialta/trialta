import * as React from "react"
import client from "../../apollo/client"
import Layout from "../../components/Templates/Layout/Layout"
import Slider from "react-slick"
import ActiveLink from "../../components/Molecules/Header/ActiveLink"
import Link from "next/link"
import { LazyLoadImage } from "react-lazy-load-image-component"
import { QUERY_PORFOLIO } from "../../apollo/queries/query-porfolio"

const Portfolio = ({ data }) => {
  let categoryUrl = ""
  const portfolio = "portafolio"

  const timing = 10000
  const settings = {
    dots: true,
    className: "center",
    centerMode: true,
    centerPadding: "0px",
    infinite: true,
    autoplay: true,
    slidesToShow: 1,
    speed: 600,
    autoplaySpeed: timing,
  }

  return (
    <Layout>
      <>
        {data?.porfolio?.nosotros?.nodes.map((data, key) => (
          <main className="trl-banner" key={key}>
            <article>
              <h2 className="trl-title">
                {data?.group?.title}
                <strong>{data?.group?.subtitle}</strong>
              </h2>
              <p className="trl-paragraph">{data?.group?.paragraph}</p>
              <figure>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data?.group?.image?.srcSet}
                  alt={data?.group?.title}
                  key={key}
                />
              </figure>
            </article>
          </main>
        ))}
        <nav className="trl-filter">
          <ul>
            <li>
              <Link href="/portafolio">
                <a className="active">Todos</a>
              </Link>
            </li>
            {data?.porfolio?.categories?.nodes.map((category) => (
              <li key={category.id}>
                <ActiveLink href={`/portafolio/${category.slug}`}>
                  <a>{category.name}</a>
                </ActiveLink>
              </li>
            ))}
          </ul>
        </nav>
        <div className="trl-grid">
          <div className="trl-projects">
            {data?.porfolio?.proyectos?.nodes?.map((data, key) => (
              <article key={key}>
                <LazyLoadImage
                  effect="blur"
                  srcSet={data?.group?.image?.srcSet}
                  alt={data?.group?.title}
                  key={key}
                />
                <div className="trl-content">
                  <div>
                    <h3>
                      {data?.categories.edges?.map((category) => {
                        categoryUrl = category?.node?.slug
                      })}
                      <Link
                        href={`${categoryUrl}/${data?.group.url}`}
                      >
                        <a>{data?.group?.title}</a>
                      </Link>
                    </h3>
                    <p>{data?.group?.subtitle}</p>
                  </div>
                </div>
              </article>
            ))}
          </div>

          <div className="trl-lines">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
        <div className="trl-testimonials">
          <h3 className="trl-title">Testimoniales</h3>
          <div className="sld-testimonials">
            <Slider {...settings}>
              {data?.porfolio?.testimoniales?.nodes.map((data, key) => (
                <article className="item" key={key}>
                  <p>{data?.group?.testimonial}</p>
                  <strong>{data?.group?.person}</strong>
                  <span>{data?.group?.occupation}</span>
                </article>
              ))}
            </Slider>
          </div>
        </div>
      </>
    </Layout>
  )
}

export default Portfolio

export async function getStaticProps() {
  //export async function getServerSideProps() {
  const { data, loading, networkStatus } = await client.query({
    query: QUERY_PORFOLIO,
  })

  return {
    props: {
      data: {
        porfolio: data,
      },
    },
    revalidate: 1,
  }
}
