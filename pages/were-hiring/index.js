import * as React from "react"
import Layout from "../../components/Templates/Layout/Layout"
import Typeform from "../../components/Organisms/Typeform/TypeForm"

const Werehiring = () => {
  return (
    <div className="page-werehiring">
      <Layout>
        <Typeform />
      </Layout>
    </div>
  )
}

export default Werehiring
